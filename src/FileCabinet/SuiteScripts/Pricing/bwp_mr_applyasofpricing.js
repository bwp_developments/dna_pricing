var FORMATMODULE, SEARCHMODULE, RECORDMODULE, RUNTIMEMODULE, PRICINGMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define(['N/format', 'N/search', 'N/record', 'N/runtime', './bwp_pricingModule'], runMapReduce);

function runMapReduce(format, search, record, runtime, pricingModule) {
	FORMATMODULE= format;
	SEARCHMODULE= search;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	PRICINGMODULE= pricingModule;
	
	var returnObj = {};
	returnObj['getInputData'] = _getInputData;
	returnObj['map'] = _map;
	returnObj['reduce'] = _reduce;
	returnObj['summarize'] = _summarize;
	return returnObj;
}

/**
 * Marks the beginning of the Map/Reduce process and generates input data.
 *
 * @typedef {Object} ObjectRef
 * @property {number} id - Internal ID of the record instance
 * @property {string} type - Record type id
 *
 * @return {Array|Object|SEARCHMODULE|RecordRef} inputSummary
 * @since 2015.1
 */
function _getInputData() {	
	log.debug('getInputData started');
	
	var nowDate = PRICINGMODULE.getNowDate();
	var nowDateString = FORMATMODULE.format({
		value: nowDate,
		type: FORMATMODULE.Type.DATE
	});
	logVar('nowDate formated', nowDateString);
	var searchObj = SEARCHMODULE.create({
		type: 'customrecord_bwp_pricingrule',
		filters: [
		['isinactive', 'is', 'F'],
		'AND',
		[
			['custrecord_bwp_pricingruleiseffective', 'is', 'T'],
			'AND',
			['custrecord_bwp_pricingruleenddate', 'before', nowDateString],
			'OR',
			// don't filter on effective, reapply all rules that should be effective in case of a user modification
//			['custrecord_bwp_pricingruleiseffective', 'is', 'F'],
//			'AND',
			['custrecord_bwp_pricingrulestartdate', 'onorbefore', nowDateString],
			'AND',
			['custrecord_bwp_pricingruleenddate', 'onorafter', nowDateString],
			'OR',
			['custrecord_bwp_pricingruleiseffective', 'is', 'T'],
			'AND',
			['custrecord_bwp_pricingrulestartdate', 'after', nowDateString],
		]
		],           
		columns: ['internalid', 'name', 'custrecord_bwp_pricingrulecustomer', 'custrecord_bwp_pricingruleitem', 'custrecord_bwp_pricingrulepricegroup',
		          'custrecord_bwp_pricingrulestartdate', 'custrecord_bwp_pricingruleenddate', 'custrecord_bwp_pricingruleiseffective']
	});
			
	return searchObj;
	
	log.debug('getInputData done');
}

/**
 * Executes when the map entry point is triggered and applies to each key/value pair.
 *
 * @param {MapSummary} context - Data collection containing the key/value pairs to process through the map stage
 * @since 2015.1
 */
function _map(context) {
	//log.debug('map started');

//	log.debug('map context', context.value);
	var contextValues = JSON.parse(context.value).values;
//	logRecord('contextValues', contextValues);
	
	// Write data into the context so the reduce can unapply before applying
	var internalId = contextValues.internalid.value;
	var customer = contextValues.custrecord_bwp_pricingrulecustomer.value;
	var item;
	if (contextValues.custrecord_bwp_pricingruleitem) {
		item = contextValues.custrecord_bwp_pricingruleitem.value;
	}
	var priceGroup;
	if (contextValues.custrecord_bwp_pricingrulepricegroup) {
		priceGroup = contextValues.custrecord_bwp_pricingrulepricegroup.value;
	}
	var startDate = FORMATMODULE.parse({
		value: contextValues.custrecord_bwp_pricingrulestartdate,
		type: FORMATMODULE.Type.DATE
	});
	var endDate = FORMATMODULE.parse({
		value: contextValues.custrecord_bwp_pricingruleenddate,
		type: FORMATMODULE.Type.DATE
	});
	
	var reduceKey = customer + '/';
	if (item) {
		reduceKey += item;
	}
	reduceKey += '/';
	if (priceGroup) {
		reduceKey += priceGroup;
	}
	var reduceValue;
	var nowDate = PRICINGMODULE.getNowDate();
	if (endDate < nowDate || startDate > nowDate) {
		reduceValue = '1.unapply:' + internalId;
	} else {
		reduceValue = '2.apply:' + internalId;
	}
	
	context.write({
		key: reduceKey,
		value: reduceValue
	});
	
	//log.debug('map done');
}

/**
 * Executes when the reduce entry point is triggered and applies to each group.
 *
 * @param {ReduceSummary} context - Data collection containing the groups to process through the reduce stage
 * @since 2015.1
 */
function _reduce(context) {
//	log.debug('reduce started');
	
	context.values.forEach(function(value) {
		logRecord('Reduce value', value);
		var internalId = value.split(':')[1];
//		logVar('internalId', internalId);
		// simply load and save the pricing rule, it will apply or unapply because of after submit event
		var pricingRuleRecord = RECORDMODULE.load({
			type: 'customrecord_bwp_pricingrule',
			id: internalId,
			isDynamic: false
		});
		pricingRuleRecord.save();
	});
	
	//log.debug('reduce done');
}


/**
 * Executes when the summarize entry point is triggered and applies to the result set.
 *
 * @param {Summary} summary - Holds statistics regarding the execution of a map/reduce script
 * @since 2015.1
 */
function _summarize(summary) {
//	log.debug('summary started');
//	log.debug('summary', summary);
	
//	log.debug('summary done');
}

function logRecord(logTitle, record) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}
    
