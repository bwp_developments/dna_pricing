var PRICINGMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['./bwp_pricingModule'], runSuitelet);

function runSuitelet(pricingModule) {
	PRICINGMODULE= pricingModule;
	var returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;	
}
   
/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} context
 * @param {ServerRequest} context.request - Encapsulation of the incoming request
 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
 * @Since 2015.2
 */
function _onRequest(context) {
	var method = context.request.method;
	//logRecord(context, 'context');
	
	log.debug(method);
	if (method == 'GET') {
		getFunction(context);
	} else if (method == 'POST') {
		postFunction(context);
	}
}

function getFunction(context) {
	log.debug({
        title: 'Function start',
        details: 'getFunction'
    });

	var record = context.request.parameters.custparam_bwp_record;
	
	var error = PRICINGMODULE.checkValidity(JSON.parse(record));
	if (error) {
		context.response.write(JSON.stringify(error));
	}
	
	log.debug({
        title: 'Function end',
        details: 'getFunction'
    });
}

function postFunction(context) {

}

function logRecord(logTitle, record) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}