var ERRORMODULE, FORMATMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE;

/**
 * bwp_pricingModule.js
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define(['N/error', 'N/format', 'N/record', 'N/runtime', 'N/search'], runModule);

function runModule(error, format, record, runtime, search) {
	ERRORMODULE= error;
	FORMATMODULE= format;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	 
	var returnObj = {};
	returnObj['checkValidity'] = checkValidityCall;
	returnObj['applyRule'] = applyRuleCall;
	returnObj['unapplyRule'] = unapplyRuleCall;
	returnObj['getNowDate'] = getNowDateCall;
	return returnObj;
}

/**
 * Returns the date used by user event and map reduce script to be sure they have the same behavior
 * The date returned is today's server date with local time zero and it corresponds to utc time T08:00:00.000Z on the same day
 * Using a hard coded date allows better testing
 * @returns {Date}
 */
function getNowDateCall() {
	// return new Date(2021, 10, 9, 0, 0, 0);
	// return new Date('2021/11/09');
	var nowDate = new Date(Date.now());
	return new Date(nowDate.getUTCFullYear(), nowDate.getUTCMonth(), nowDate.getUTCDate(), 0, 0, 0);
}

/**
 * This function can be called from server script or client script (thru a suitelet)
 * When called from client script the record parameter is a custom object that requires specific functions 
 * (the API record module does not work)
 * When called from server script the record parameter is a custom record of type customrecord_bwp_pricingrule
 * @param record
 * @returns
 */
function checkValidityCall(record) {
	log.debug({
		title: 'Function started',
		details: 'checkValidityCall'
	});
	var internalid, customer, item, group, startDate, endDate, priceLevel;
//	logRecord('record', record);
	if (record.type) {
		internalid = record.getValue({ fieldId: 'id'});
		customer = record.getValue({ fieldId: 'custrecord_bwp_pricingrulecustomer'});
		item = record.getValue({ fieldId: 'custrecord_bwp_pricingruleitem'});
		group = record.getValue({ fieldId: 'custrecord_bwp_pricingrulepricegroup'});
		startDate = record.getValue({ fieldId: 'custrecord_bwp_pricingrulestartdate'});
		endDate = record.getValue({ fieldId: 'custrecord_bwp_pricingruleenddate'});
		priceLevel = record.getValue({ fieldId: 'custrecord_bwp_pricingrulepricelevel'});
	} else {
		internalid = parseClientRecord(record, 'id');
		customer = parseClientRecord(record, 'custrecord_bwp_pricingrulecustomer');
		item = parseClientRecord(record, 'custrecord_bwp_pricingruleitem');
		group = parseClientRecord(record, 'custrecord_bwp_pricingrulepricegroup');
		startDate = parseClientRecord(record, 'custrecord_bwp_pricingrulestartdate');
		endDate = parseClientRecord(record, 'custrecord_bwp_pricingruleenddate');
		priceLevel = parseClientRecord(record, 'custrecord_bwp_pricingrulepricelevel');
	}
//	logVar('internalid', internalid);
//	logVar('customer', customer);
//	logVar('item', item);
//	logVar('group', group);
//	logVar('startDate', startDate);
//	logVar('endDate', endDate);
//	logVar('priceLevel', priceLevel);
	
	if (!customer) {
		return ERRORMODULE.create({
			name: 'BWP_INVALID_DATA',
			message: 'Customer field requires a value'
		});
	}
	
	if (!priceLevel) {
		return ERRORMODULE.create({
			name: 'BWP_INVALID_DATA',
			message: 'Price level field requires a value'
		});
	}
	
	if (item && group) {
		return ERRORMODULE.create({
			name: 'BWP_INVALID_DATA',
			message: 'Item field and Group field can\'t be populated together'
		});
	}
	
	if (endDate < startDate) {
		return ERRORMODULE.create({
			name: 'BWP_INVALID_DATA',
			message: 'End date must be greater than or equal to start date'
		});
	}
	
	var ruleInConflict = checkPeriodConflict(internalid, customer, item, group, startDate, endDate);
	if (ruleInConflict) {
		return ERRORMODULE.create({
			name: 'BWP_PERIOD_CONFLICT',
			message: 'This pricing rule is in conclict with an existing one (id ' + ruleInConflict + ') due to dates of validity'
		});
	}
}

function applyRuleCall(recordId) {
	log.debug({
		title: 'Function started',
		details: 'applyRuleCall for ' + recordId
	});
	var pricingRuleRecord = RECORDMODULE.load({
		type: 'customrecord_bwp_pricingrule',
		id: recordId,
		isDynamic: false
	});
	
	// Don't apply an inactive rule
	if (pricingRuleRecord.getValue({ fieldId: 'isinactive' })) {
		return;
	}
	
	var customer = pricingRuleRecord.getValue({ fieldId: 'custrecord_bwp_pricingrulecustomer' });
	var item = pricingRuleRecord.getValue({ fieldId: 'custrecord_bwp_pricingruleitem' });
	var group = pricingRuleRecord.getValue({ fieldId: 'custrecord_bwp_pricingrulepricegroup' });
	var priceLevel = pricingRuleRecord.getValue({ fieldId: 'custrecord_bwp_pricingrulepricelevel' });
	var isEffective = pricingRuleRecord.getValue({ fieldId: 'custrecord_bwp_pricingruleiseffective' });
	
	// Check pricelevel is not inactive
	if (priceLevel) {
		var priceLevelFieldsValues = SEARCHMODULE.lookupFields({
			type: SEARCHMODULE.Type.PRICE_LEVEL,
			id: priceLevel,
			columns: ['isinactive', 'name']
		});
		
		if (priceLevelFieldsValues.isinactive) {
			pricingRuleRecord.setValue({
				fieldId: 'custrecord_bwp_pricingruleerrorflag',
				value: true
			});
			pricingRuleRecord.setValue({
				fieldId: 'custrecord_bwp_pricingruleerrormsg',
				value: 'Price level ' + priceLevelFieldsValues.name + ' (' + priceLevel + ') is inactive'
			});
			pricingRuleRecord.save();
			log.debug({
				title: 'Function done',
				details: 'applyRuleCall'
			});
			return;		
		}
	}
	
	// Check pricegroup is not inactive
	if (group) {
		var groupFieldsValues = SEARCHMODULE.lookupFields({
			type: SEARCHMODULE.Type.PRICING_GROUP,
			id: group,
			columns: ['isinactive', 'name']
		});
		
		if (groupFieldsValues.isinactive) {
			pricingRuleRecord.setValue({
				fieldId: 'custrecord_bwp_pricingruleerrorflag',
				value: true
			});
			pricingRuleRecord.setValue({
				fieldId: 'custrecord_bwp_pricingruleerrormsg',
				value: 'Pricing group ' + groupFieldsValues.name + ' (' + group + ') is inactive'
			});
			pricingRuleRecord.save();
			log.debug({
				title: 'Function done',
				details: 'applyRuleCall'
			});
			return;		
		}
		
	}

	// Check item is not inactive
	if (item) {
		var itemFieldsValues = SEARCHMODULE.lookupFields({
			type: SEARCHMODULE.Type.ITEM,
			id: item,
			columns: ['isinactive', 'name', 'displayname']
		});
		
		if (itemFieldsValues.isinactive) {
			pricingRuleRecord.setValue({
				fieldId: 'custrecord_bwp_pricingruleerrorflag',
				value: true
			});
			pricingRuleRecord.setValue({
				fieldId: 'custrecord_bwp_pricingruleerrormsg',
				value: 'Item ' + itemFieldsValues.name + ' ' + itemFieldsValues.displayname + ' (' + item + ') is inactive'
			});
			pricingRuleRecord.save();
			log.debug({
				title: 'Function done',
				details: 'applyRuleCall'
			});
			return;		
		}		
	}
	
	var customerRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.CUSTOMER,
		id: customer,
		isDynamic: true
	});
	var customerIsModified = false;
	var isPricingRecordSaved = false;
//	logRecord('customerRecord', customerRecord);
	if (!item && !group) {
		var customerPriceLevel = customerRecord.getValue({ fieldId: 'pricelevel'});
		if (customerPriceLevel != priceLevel) {
			customerRecord.setValue({ 
				fieldId: 'pricelevel',
				value: priceLevel
			});
			customerIsModified = true;
		}		
	} else if (item) {
		var lineNumber = customerRecord.findSublistLineWithValue({
			sublistId: 'itempricing',
			fieldId: 'item',
			value: item
		});
		if (lineNumber < 0) {
			var isOK = true;
			// add item price level line 
			customerRecord.selectNewLine({
				sublistId: 'itempricing'
			});
			try {
				customerRecord.setCurrentSublistValue({
					sublistId: 'itempricing',
					fieldId: 'item',
					value: item
				});				
			} catch (ex) {
				isOK = false;
				var fieldsValues = SEARCHMODULE.lookupFields({
					type: SEARCHMODULE.Type.ITEM,
					id: item,
					columns: ['name', 'displayname']
				});

				pricingRuleRecord.setValue({
					fieldId: 'custrecord_bwp_pricingruleiseffective',
					value: false
				});
				pricingRuleRecord.setValue({
					fieldId: 'custrecord_bwp_pricingruleerrorflag',
					value: true
				});
				pricingRuleRecord.setValue({
					fieldId: 'custrecord_bwp_pricingruleerrormsg',
					value: 'Item ' + fieldsValues.name + ' ' + fieldsValues.displayname + ' is not valid for Item Pricing'
				});
				pricingRuleRecord.save();
				isPricingRecordSaved = true;
			}
			
			if (isOK) {
				customerRecord.setCurrentSublistValue({
					sublistId: 'itempricing',
					fieldId: 'level',
					value: priceLevel
				});
				customerIsModified = true;				
			}
		} else {
			var itemPriceLevel = customerRecord.getSublistValue({
				sublistId: 'itempricing',
				fieldId: 'level',
				line: lineNumber
			});
			if (itemPriceLevel != priceLevel) {
				customerRecord.selectLine({
					sublistId: 'itempricing',
					line: lineNumber
				});
				customerRecord.setCurrentSublistValue({
					sublistId: 'itempricing',
					fieldId: 'level',
					value: priceLevel
				});
				customerIsModified = true;
			}
		}
		if (customerIsModified) {
			customerRecord.commitLine({
				sublistId: 'itempricing'				
			});
		}
	} else if (group) {
		var lineNumber = customerRecord.findSublistLineWithValue({
			sublistId: 'grouppricing',
			fieldId: 'group',
			value: group
		});
		if (lineNumber < 0) {
			// add group price level line 
			customerRecord.selectNewLine({
				sublistId: 'grouppricing'
			});
			customerRecord.setCurrentSublistValue({
				sublistId: 'grouppricing',
				fieldId: 'group',
				value: group
			});
			customerRecord.setCurrentSublistValue({
				sublistId: 'grouppricing',
				fieldId: 'level',
				value: priceLevel
			});
			customerIsModified = true;
		} else {
			var groupPriceLevel = customerRecord.getSublistValue({
				sublistId: 'grouppricing',
				fieldId: 'level',
				line: lineNumber
			});
			if (groupPriceLevel != priceLevel) {
				customerRecord.selectLine({
					sublistId: 'grouppricing',
					line: lineNumber
				});
				customerRecord.setCurrentSublistValue({
					sublistId: 'grouppricing',
					fieldId: 'level',
					value: priceLevel
				});
				customerIsModified = true;
			}
		}
		if (customerIsModified) {
			customerRecord.commitLine({
				sublistId: 'grouppricing'				
			});
		}		
	}

	if (customerIsModified) {
		customerRecord.save();
	}
	if (!isPricingRecordSaved && !isEffective) {
		pricingRuleRecord.setValue({
			fieldId: 'custrecord_bwp_pricingruleiseffective',
			value: true
		});
		pricingRuleRecord.setValue({
			fieldId: 'custrecord_bwp_pricingruleerrorflag',
			value: false
		});
		pricingRuleRecord.setValue({
			fieldId: 'custrecord_bwp_pricingruleerrormsg',
			value: ''
		});
		pricingRuleRecord.save();		
	}	
	log.debug({
		title: 'Function done',
		details: 'applyRuleCall'
	});
}

function unapplyRuleCall(recordId, customer, item, group) {
	log.debug({
		title: 'Function started',
		details: 'unapplyRuleCall for ' + recordId + '/' + customer + '/' + item + '/' + group
	});
	var pricingRuleRecord;
	if (recordId) {
		pricingRuleRecord = RECORDMODULE.load({
			type: 'customrecord_bwp_pricingrule',
			id: recordId,
			isDynamic: false
		});
		
	}
	var customerRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.CUSTOMER,
		id: customer,
		isDynamic: true
	});
	var customerIsModified = false;
	
	if (!item && !group) {
		customerRecord.setValue({ 
			fieldId: 'pricelevel',
			value: ''
		});
		customerIsModified = true;
	} else if (item) {
		var lineNumber = customerRecord.findSublistLineWithValue({
			sublistId: 'itempricing',
			fieldId: 'item',
			value: item
		});
		if (lineNumber >= 0) {
			customerRecord.removeLine({
				sublistId: 'itempricing',
				line: lineNumber
			});
			customerIsModified = true;
		}
	} else if (group) {
		var lineNumber = customerRecord.findSublistLineWithValue({
			sublistId: 'grouppricing',
			fieldId: 'group',
			value: group
		});
		if (lineNumber >= 0) {
			customerRecord.removeLine({
				sublistId: 'grouppricing',
				line: lineNumber
			});
			customerIsModified = true;
		}		
	}

	if (customerIsModified) {
		customerRecord.save();
	}
	
	if (pricingRuleRecord) {
		var isEffective = pricingRuleRecord.getValue({ fieldId: 'custrecord_bwp_pricingruleiseffective' });
		if (isEffective) {
			pricingRuleRecord.setValue({
				fieldId: 'custrecord_bwp_pricingruleiseffective',
				value: false
			});
			pricingRuleRecord.setValue({
				fieldId: 'custrecord_bwp_pricingruleerrorflag',
				value: false
			});
			pricingRuleRecord.setValue({
				fieldId: 'custrecord_bwp_pricingruleerrormsg',
				value: ''
			});
			pricingRuleRecord.save();			
		}			
	}
	log.debug({
		title: 'Function done',
		details: 'unapplyRuleCall'
	});
}

function checkPeriodConflict(internalid, customer, item, group, startDate, endDate) {
	var searchFilters = [
		['custrecord_bwp_pricingrulecustomer', 'anyof', customer], 
		'AND', 
		['custrecord_bwp_pricingruleitem', 'anyof', item || '@NONE@'], 
		'AND',
		['custrecord_bwp_pricingrulepricegroup', 'anyof', group || '@NONE@'],
		'AND', 
		['custrecord_bwp_pricingrulestartdate', 'onorbefore', formatDateForSearch(endDate)], 
		'AND', 
		['custrecord_bwp_pricingruleenddate', 'onorafter', formatDateForSearch(startDate)], 
		'AND', 
		['isinactive', 'is', 'F']
	];
	if (internalid) {
		searchFilters.push('AND');
		searchFilters.push(['internalid', 'noneof', internalid]);
	}
	
	var searchObj = SEARCHMODULE.create({
		type: 'customrecord_bwp_pricingrule',
		columns: ['internalid'],
		filters: searchFilters
	});
	var pagedDataObj = searchObj.runPaged();
	if (pagedDataObj.count > 0) {
//		log.debug ('overlap');
		return pagedDataObj.fetch({ index: 0 }).data[0].getValue({ name: 'internalid' });
	} else {
//		log.debug('no overlap');
		return false;
	}
}

function parseClientRecord(record, field) {
	switch (field) {
		case 'custrecord_bwp_pricingrulestartdate':
			return parseClientDate(record[field]);
		case 'custrecord_bwp_pricingruleenddate':
			return parseClientDate(record[field]);
		default:
			return record[field];			
	}
}

/**
 * Parse string date sent by a client script (ISO format, UTC zero)
 * @param stringDate in ISO format with utc zero
 * @returns
 */
function parseClientDate(stringDate) {
	// Date is sent by client script as ISO date with utc zero
	return new Date(stringDate.split('T')[0].replace(/-/g, '/'));
}

/**
 * Format a date so it is compatible with search filters
 * @param dateValue
 * @returns
 */	
function formatDateForSearch(dateValue) {
	var formattedDate = FORMATMODULE.format({
		value: dateValue,
		type: FORMATMODULE.Type.DATE
	});
	return formattedDate;
}

function logRecord(logTitle, record) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}