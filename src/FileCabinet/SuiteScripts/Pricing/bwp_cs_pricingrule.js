var CURRENTRECORDMODULE, FORMATMODULE, URLMODULE, HTTPSMODULE, UIDIALOGMODULE;

/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * Script to deploy on customrecord_bwp_pricingrule custom record
 */
define(['N/currentRecord', 'N/format', 'N/url', 'N/https', 'N/ui/dialog'], runClient);

function runClient(currentRecord, format, url, https, uidialog) {
	CURRENTRECORDMODULE= currentRecord;
	FORMATMODULE= format;
	URLMODULE= url;
	HTTPSMODULE= https;
	UIDIALOGMODULE= uidialog;
	
	var returnObj = {};
//	returnObj['pageInit'] = _pageInit;
//	returnObj['fieldChanged'] = _fieldChanged;
//	returnObj['postSourcing'] = _postSourcing;
//	returnObj['sublistChanged'] = _sublistChanged;
//	returnObj['lineInit'] = _lineInit;
	returnObj['validateField'] = _validateField;
//	returnObj['validateLine'] = _validateLine;
//	returnObj['validateInsert'] = _validateInsert;
//	returnObj['validateDelete'] = _validateDelete;
	returnObj['saveRecord'] = _saveRecord;
	return returnObj;
}

/**
 * Function to be executed after page is initialized.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
 *
 * @since 2015.2
 */
function _pageInit(scriptContext) {

}

/**
 * Function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @since 2015.2
 */
function _fieldChanged(scriptContext) {

}

/**
 * Function to be executed when field is slaved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 *
 * @since 2015.2
 */
function _postSourcing(scriptContext) {

}

/**
 * Function to be executed after sublist is inserted, removed, or edited.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _sublistChanged(scriptContext) {

}

/**
 * Function to be executed after line is selected.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _lineInit(scriptContext) {

}

/**
 * Validation function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @returns {boolean} Return true if field is valid
 *
 * @since 2015.2
 */
function _validateField(scriptContext) {
	if (scriptContext.fieldId == 'custrecord_bwp_pricingrulecustomer') {
//		alert('_validateField ' + JSON.stringify(scriptContext));
		if (scriptContext.currentRecord.id) {
			UIDIALOGMODULE.alert({
				title: 'Invalid action',
				message: 'Customer field may not change'
			});
			return false;
		}
	}
	return true;
}

/**
 * Validation function to be executed when sublist line is committed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateLine(scriptContext) {

}

/**
 * Validation function to be executed when sublist line is inserted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateInsert(scriptContext) {

}

/**
 * Validation function to be executed when record is deleted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateDelete(scriptContext) {

}

/**
 * Validation function to be executed when record is saved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @returns {boolean} Return true if record is valid
 *
 * @since 2015.2
 */
function _saveRecord(scriptContext) {
	var props = ['id', 'custrecord_bwp_pricingrulecustomer', 'custrecord_bwp_pricingruleitem', 'custrecord_bwp_pricingrulepricegroup', 
	             'custrecord_bwp_pricingrulestartdate', 'custrecord_bwp_pricingruleenddate', 'custrecord_bwp_pricingrulepricelevel',
	             'custrecord_bwp_pricingruleiseffective', 'custrecord_bwp_pricingruleerrorflag', 'custrecord_bwp_pricingruleerrormsg',
	             'custrecord_bwp_pricingrulerulescope'];
	var record = {};
	props.forEach(function(prop) {
		var propValue = scriptContext.currentRecord.getValue({ fieldId: prop });
		if (util.isDate(propValue)) {
			propValue = utcZeroDate(propValue);
		}
		record[prop] = propValue;
	});
	var suiteletUrl = URLMODULE.resolveScript({
		scriptId: 'customscript_bwp_sl_validatepricingrule',
		deploymentId: 'customdeploy_bwp_sl_validatepricingrule',
		params: {
			'custparam_bwp_record': JSON.stringify(record)
		}
	});
	var response = HTTPSMODULE.get({
		url: suiteletUrl
	});
	
	if (response.body) {
		var responseBody = JSON.parse(response.body);
		UIDIALOGMODULE.alert({
			title: 'Invalid record',
			message: responseBody.message
		});
		return false;
	}
	
	return true;
}

/**
 * 
 * @param {Date} clientDate 
 * @returns 
 */
function utcZeroDate(clientDate) {
	// Dates are created with time 00:00:00 with the user's timezone. The conversion to UTC date can change the date
	// For example 01/01/2021 with GMT+1 timezone becomes 2020-12-31T23:00:00.000Z ==> the wrong date is sent to the server
	// ==> calculate utc date with time 00:00:00
	return new Date(clientDate.getTime() - clientDate.getTimezoneOffset()*60000);
	// ==> Better solution: return 'YYYY/MM/DD' --> on server side just do 'new Date('YYYY/MM/DD')
}

/**
 * 
 * @param {Date} clientDate 
 * @returns 
 */
 function utcZeroDateOld(clientDate) {
	// Dates are created with time 00:00:00 with the user's timezone. The conversion to UTC date can change the date
	// For example 01/01/2021 with GMT+1 timezone becomes 2020-12-31T23:00:00.000Z ==> the wrong date is sent to the server
	// ==> calculate utc date with time 00:00:00
	return (new Date(clientDate.getTime() - clientDate.getTimezoneOffset()*60000)).toISOString();
}
