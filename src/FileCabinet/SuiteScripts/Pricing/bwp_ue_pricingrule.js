var ERRORMODULE, RECORDMODULE, SEARCHMODULE, UISERVERWIDGETMODULE, PRICINGMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/error', 'N/record', 'N/search', 'N/ui/serverWidget', './bwp_pricingModule'], runUserEvent);

function runUserEvent(error, record, search, serverWidget, pricingModule) {
	ERRORMODULE= error;
	RECORDMODULE= record;
	SEARCHMODULE= search;
	UISERVERWIDGETMODULE= serverWidget;
	PRICINGMODULE= pricingModule;
	
	var returnObj = {};
//	returnObj['beforeLoad'] = _beforeLoad;
	returnObj['beforeSubmit'] = _beforeSubmit;
	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('beforeLoad started');
	
	log.debug('beforeLoad done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');
	logRecord('scriptContext', scriptContext);
	
	if (scriptContext.type == scriptContext.UserEventType.DELETE) {
		log.debug('beforeSubmit done');
		return;
	}
	
	// Customer is not allowed to change
	var customerFieldValues = readFieldValuesFromScriptContext(scriptContext, 'custrecord_bwp_pricingrulecustomer');
	if (customerFieldValues.hasChanged) {
		throw ERRORMODULE.create({
			name: 'BWP_INVALID_ACTION',
			message: 'Customer field must remain unchanged'
		});
	}
	
	var recordObj = scriptContext.newRecord;
	if (scriptContext.type == scriptContext.UserEventType.XEDIT) {
		// Don't work with oldRecord, because the modifications will also apply to afterSubmit entrypoint
		// recordObj = mixNewAndOldRecords(scriptContext.newRecord, scriptContext.oldRecord);

		// Because it is beforeSubmit, load old record by reading it again with record module
		var oldRecordObj = RECORDMODULE.load({
			type: scriptContext.newRecord.type,
			id: scriptContext.newRecord.id,
			isDynamic: false
		});
		recordObj = mixNewAndOldRecords(scriptContext.newRecord, oldRecordObj);
	}
	var error = PRICINGMODULE.checkValidity(recordObj);
	if (error) {
		throw error;
	}
	
	// Calculate priceScope field value
	var item = readFieldValuesFromScriptContext(scriptContext, 'custrecord_bwp_pricingruleitem').newValue;
	var group = readFieldValuesFromScriptContext(scriptContext, 'custrecord_bwp_pricingrulepricegroup').newValue;
//	logVar('item', item);
//	logVar('group', group);
	var priceScope = 1;
	if (group) {
		priceScope = 2;
	}
	if (item) {
		priceScope = 3;
	}
//	logVar('priceScope', priceScope);	
	scriptContext.newRecord.setValue({
		fieldId: 'custrecord_bwp_pricingrulerulescope',
		value: priceScope
	});
	
	log.debug('beforeSubmit done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {
	log.debug('afterSubmit started');
	logRecord('scriptContext', scriptContext);
	logVar('Isinactive', scriptContext.newRecord.getValue({ fieldId: 'isinactive'}));
	var isApplyRequired = false;
	var isUnapplyRequired = false;

	// Because of behaviour of scriptContext.newRecord in case of XEDIT it is more simple to send the id of 
	// the record to the module that will load it
	var recordId = scriptContext.newRecord.getValue({ fieldId: 'id'});
	
	// No oldCustomer, customer should not change
	var customer = readFieldValuesFromScriptContext(scriptContext, 'custrecord_bwp_pricingrulecustomer').newValue;
	var itemFieldValues = readFieldValuesFromScriptContext(scriptContext, 'custrecord_bwp_pricingruleitem');
	var groupFieldValues = readFieldValuesFromScriptContext(scriptContext, 'custrecord_bwp_pricingrulepricegroup');
	var isInactiveFieldValues = readFieldValuesFromScriptContext(scriptContext, 'isinactive');
	logRecord('isInactiveFieldValues', isInactiveFieldValues);
	
	// Detect if pricing rule should be applied or unapplied or both of them or nothing
	switch (scriptContext.type) {
		case scriptContext.UserEventType.DELETE:
			if (isRuleEffective(scriptContext)) {
				log.debug('Unapply deleted rule');
				logRecord('Deleted rule', scriptContext.oldRecord);
				isUnapplyRequired = true;
				// Reset the record id so the program does not try to update the status on a record that does not exist
				recordId = '';
			}
			break;
		case scriptContext.UserEventType.CREATE:
			var startDate = scriptContext.newRecord.getValue({ fieldId: 'custrecord_bwp_pricingrulestartdate' });
			var endDate = scriptContext.newRecord.getValue({ fieldId: 'custrecord_bwp_pricingruleenddate' });
			var nowDate = new Date(Date.now());
			if (isValidityDatesOnToday(startDate, endDate)) {
				log.debug('Apply newly created rule');
				isApplyRequired = true;
			}
			break;
		case scriptContext.UserEventType.XEDIT:
		case scriptContext.UserEventType.EDIT:
			var startDateFieldValues = readFieldValuesFromScriptContext(scriptContext, 'custrecord_bwp_pricingrulestartdate');
			var endDateFieldValues = readFieldValuesFromScriptContext(scriptContext, 'custrecord_bwp_pricingruleenddate');
			if (isRuleEffective(scriptContext)) {
				if (!isValidityDatesOnToday(startDateFieldValues.newValue, endDateFieldValues.newValue)) {
					log.debug('Unapply old rule because of dates no longer valid');
					isUnapplyRequired = true;
				}
				// Unapply rule in case of data change
				if (itemFieldValues.hasChanged || groupFieldValues.hasChanged || 
						isInactiveFieldValues.hasChanged && isInactiveFieldValues.newValue) {
					log.debug('Unapply old rule because of data change');
					isUnapplyRequired = true;
				}
			}
			
			// Apply newRecord definition if required
			if (isValidityDatesOnToday(startDateFieldValues.newValue, endDateFieldValues.newValue) && !isInactiveFieldValues.newValue) {
				log.debug('Apply new rule definition');
				isApplyRequired = true;
			}
			break;
	}
	
	if (isUnapplyRequired) {
		PRICINGMODULE.unapplyRule(recordId, customer, itemFieldValues.oldValue, groupFieldValues.oldValue);
	}
	
	if (isApplyRequired) {
		PRICINGMODULE.applyRule(recordId);
	}
		
	log.debug('afterSubmit done');
}

function isValidityDatesOnToday(startDate, endDate) {
	log.debug({
		title: 'Function start',
		details: 'isValidityDatesOnToday'
	});
	var nowDate = PRICINGMODULE.getNowDate();
	// logVar('nowDate string', nowDate.toString());
	// logVar('nowDate UTC', nowDate.toUTCString());
	// logVar('startDate string', startDate.toString());
	// logVar('startDate UTC', startDate.toUTCString());
//	logVar('endDate', endDate);
	if (startDate <= nowDate && endDate >= nowDate) {
		return true;
	}
	return false;
}

function isRuleEffective(scriptContext) {
	return readFieldValuesFromScriptContext(scriptContext, 'custrecord_bwp_pricingruleiseffective').newValue;
}

/**
 * Because of XEDIT, return field value from oldRecord in case it does not exist in newRecord
 * @param scriptContext
 * @param fieldId
 * @returns
 */
function readFieldValuesFromScriptContext(scriptContext, fieldId) {
	var fieldValue = {
		oldValue: (scriptContext.oldRecord || scriptContext.newRecord).getValue({ fieldId: fieldId }),
		newValue: '',
		hasChanged: false
	}
//	logVar('fieldId', fieldId);
//	logRecord('scriptContext.newRecord', scriptContext.newRecord);
	// Warning, isinactive is not present in newRecord when a record goes from inactive to active
	if (scriptContext.newRecord.getFields().includes(fieldId) || fieldId == 'isinactive') {
		fieldValue.newValue = scriptContext.newRecord.getValue({ fieldId: fieldId });
		fieldValue.hasChanged = (fieldValue.newValue.toString() != fieldValue.oldValue.toString());
	} else {
		fieldValue.newValue = fieldValue.oldValue;
	}
	return fieldValue;
}

function mixNewAndOldRecords(newRecord, oldRecord) {
	var fields = newRecord.getFields();
	var systemFields = ['sys_id','rectype','singleparam','id'];
	fields.forEach(function(field) {
		if (systemFields.includes(field)) { return; }
		oldRecord.setValue({
			fieldId: field,
			value: newRecord.getValue({ fieldId: field })
		});
	});
	return oldRecord;
}

function logRecord(logTitle, record) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}